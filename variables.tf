variable "cluster_id" {
    description = "ID del Cluster de OpenShift"
}

variable "ibmcloud_api_key" {
    description = "API Key de IBM Cloud"
}