data "ibm_container_cluster_config" "cluster_config" {
  cluster_name_id   = var.cluster_id

  # Required for getting the calico configuration
  admin           = "true"
  network         = "true"
  config_dir      = "/tmp"
}

resource "null_resource" "delete-deployment" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/delete_elements.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "deployment,service" "topaz-batch"
        EOT
        on_failure = continue
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}
